--------------------------------------------------------------------------------
# TP1 - INTRODUCTION TO GIT AND C LANGUAGE
--------------------------------------------------------------------------------


## Git

### Look what is git, try mini exercice online

1. Look at the official website, generals commands here: https://git-scm.com/docs/gittutorial
2. try some exercices (minimum the introduction to git) here: https://learngitbranching.js.org/?locale=fr_FR

### Now ! It's for real.

1. Initilize the (local) git repository for the exercices
2. add to git a README file (with something inside)
3. Make your first commit

### go further: Gitlab/Github !

1. Go to Gitlab or Github :  
  i. create and account  
  ii. set up your sshkey (locally and on gitlab)  
  iii. create a project  
2. Configure your repository to add a remote git project link to local repo
3. Push your work on the server and check the results on gitlab


## C Language

### Basics

1. The Language and source code:  
  i. What is C programming: https://www.guru99.com/c-programming-language.html#1  
  ii. Cheat sheet: https://cheatography.com/ashlyn-black/cheat-sheets/c-reference/  
2. The toolchain (in order to build an executable):  
  i. Makefiles : https://www.cs.colby.edu/maxwell/courses/tutorials/maketutor/  
  ii. GNU C Compiler: perso.univ-lyon1.fr/jean-claude.iehl/Public/educ/gcc.html  


## Exercice: the power4

### Init



